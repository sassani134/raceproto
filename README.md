# Jeu de Course Prototype

Ce jeu de course prototype est développé dans Godot Engine 4 en utilisant des assets de Kenney. Il s'agit d'un projet simple visant à démontrer les fonctionnalités de base d'un jeu de course.

## Aperçu du Jeu

Prochainement

## Fonctionnalités

- Contrôles de Personnages
- HUD affichant les informations de course

## Instructions d'Installation

1. Assurez-vous d'avoir Godot Engine 4 installé sur votre système.
2. Clonez ce dépôt : `git clone https://github.com/votre-utilisateur/nom-du-repo.git`
3. Ouvrez le projet dans Godot Engine 4.

## Contrôles du Jeu

- **Prochainement

## Crédits

- **Godot Engine 4:** Moteur de jeu open-source.
- **Kenney:** Assets graphiques utilisés dans le jeu.
- **Kenney:** Assets sonnore utilisés dans le jeu.



## Licence

Ce projet est sous licence [Insérer la licence ici].

---

© @sassani134 | 

